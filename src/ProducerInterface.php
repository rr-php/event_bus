<?php

namespace RR\EventBus;

/**
 * Interface ProducerInterface
 * @package RR\EventBus
 */
interface ProducerInterface
{
    /**
     * ProducerInterface constructor.
     *
     * @param array $config
     */
    public function __construct(array $config);

    /**
     * @param string $route
     * @param $message
     *
     * @return void
     */
    public function produce(string $route, $message);
}
