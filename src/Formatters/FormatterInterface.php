<?php

namespace RR\EventBus\Formatters;

/**
 * Interface FormatterInterface
 * @package RR\EventBus\Formatters
 */
interface FormatterInterface
{
    /**
     * @param mixed $payload
     *
     * @return string
     */
    public function encode($payload): string;

    /**
     * @param string $payload
     *
     * @return mixed
     */
    public function decode(string $payload);
}
