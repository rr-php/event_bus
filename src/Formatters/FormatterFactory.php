<?php

namespace RR\EventBus\Formatters;

/**
 * Class FormatterFactory
 * @package RR\EventBus\Formatters
 */
class FormatterFactory
{
    /**
     * @var array
     */
    public $availableFormatters = [
        'json' => Json::class,
    ];

    /**
     * @var string
     */
    public $defaultFormatter = 'json';

    /**
     * @param string $format
     *
     * @return bool
     */
    public function hasFormat(string $format)
    {
        return isset($this->availableFormatters[$format]);
    }

    /**
     * @param string $format
     *
     * @return FormatterInterface
     */
    public function instance(string $format): FormatterInterface
    {
        $class = $this->availableFormatters[$format];

        return new $class();
    }
}
