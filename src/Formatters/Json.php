<?php

namespace RR\EventBus\Formatters;

/**
 * Class Json
 * @package RR\EventBus\Formatters
 */
class Json implements FormatterInterface
{
    /**
     * @param mixed $payload
     *
     * @return string
     */
    public function encode($payload): string
    {
        return json_encode($payload);
    }

    /**
     * @param string $payload
     *
     * @return mixed
     */
    public function decode(string $payload)
    {
        return json_decode($payload, true);
    }
}
