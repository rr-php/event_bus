<?php

namespace RR\EventBus;

use RR\EventBus\Transports\Kafka\Consumer as KafkaConsumer;
use RR\EventBus\Transports\Kafka\Producer as KafkaProducer;
use RR\EventBus\Transports\Null\Consumer as NullConsumer;
use RR\EventBus\Transports\Null\Producer as NullProducer;

class Transports
{
    public static function map()
    {
        return [
            'kafka' => [
                'consumer' => KafkaConsumer::class,
                'producer' => KafkaProducer::class,
            ],
            'null' => [
                'consumer' => NullConsumer::class,
                'producer' => NullProducer::class,
            ]
        ];
    }
}