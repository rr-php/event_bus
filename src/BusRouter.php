<?php

namespace RR\EventBus;

use Exception;

/**
 * Class BusRouter
 * @package App\Base\Bus
 */
class BusRouter
{
    /**
     * @var callable
     */
    public $handlerCreator;

    /**
     * @var array
     */
    public $routes = [];

    /**
     * @param $topic
     * @param $handler
     * @throws Exception
     */
    public function add($topic, $handler)
    {
        $this->routes[$topic] = $handler;
    }

    /**
     * @param $route
     * @return bool
     */
    public function get($route)
    {
        return $this->routes[$route];
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return array_keys($this->routes);
    }

    /**
     * @param callable $function
     */
    public function setHandlerCreator(callable $function)
    {
        $this->handlerCreator = $function;
    }

    /**
     * @return callable
     */
    public function getHandlerCreator()
    {
        return $this->handlerCreator;
    }
}
