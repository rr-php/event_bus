<?php

namespace RR\EventBus\Transports\Kafka;

use RdKafka\Conf;
use RdKafka\TopicConf;

/**
 * Trait KafkaConfiguration
 * @package RR\EventBus\Transports\Kafka
 */
trait KafkaConfiguration
{
    /**
     * @param $config
     *
     * @return Conf
     */
    public function getGlobalConfiguration($config): Conf
    {
        $conf = new Conf();

        if (isset($config['rdkafka_conf']) && is_array($config['rdkafka_conf'])) {
            foreach ($config['rdkafka_conf'] as $k => $v) {
                $conf->set($k, $v);
            }
        }

        return $conf;
    }

    public function getTopicConfiguration($config): TopicConf
    {
        $conf = new TopicConf();

        if (isset($config['topic_conf']) && is_array($config['topic_conf'])) {
            foreach ($config['topic_conf'] as $k => $v) {
                $conf->set($k, $v);
            }
        }


        return $conf;
    }
}
