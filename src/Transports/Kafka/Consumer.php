<?php

namespace RR\EventBus\Transports\Kafka;

use Exception;
use RdKafka\Conf;
use RdKafka\KafkaConsumer;
use RdKafka\Message as RDMessage;
use RR\EventBus\BusRouter;
use RR\EventBus\Formatters\FormatterFactory;
use RR\EventBus\Message;
use RR\EventBus\Transports\AbstractConsumer;

/**
 * Class Consumer
 * @package RR\EventBus\Transports\Kafka
 */
class Consumer extends AbstractConsumer
{
    use KafkaConfiguration;

    /**
     * @var FormatterFactory $formatterFactory
     */
    protected $formatterFactory;

    /**
     * Consumer constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->formatterFactory = new FormatterFactory();

        parent::__construct($config);
    }

    /**
     * @param BusRouter $busRouter
     *
     * @throws Exception
     * @return void
     */
    public function consume(BusRouter $busRouter)
    {
        $consumer = new KafkaConsumer($this->configure());

        $consumer->subscribe($this->getTransportRoutes($busRouter));

        while (true) {
            /** @var RDMessage $message */
            $message = $consumer->consume($this->config['timeout']);
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    $result = $this->processMessage($this->processRDKafkaMessage($message), $busRouter);
                    if (!empty($this->config['rdkafka_conf']['enable.auto.commit'])
                        && $this->config['rdkafka_conf']['enable.auto.commit'] === 'false'
                        && $result) {
                        $consumer->commit($message);
                    }
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    break;
                default:
                    throw new Exception($message->errstr(), $message->err);
                    break;
            }
        }
    }

    /**
     * @return Conf
     * @throws Exception
     */
    protected function configure(): Conf
    {
        if (empty($this->config['timeout'] || !is_int($this->config['timeout']))) {
            throw new Exception('"timeout" is not set');
        }

        return $this->getGlobalConfiguration($this->config);
    }

    /**
     * @param RDMessage $message
     *
     * @return Message
     */
    protected function processRDKafkaMessage(RDMessage $message): Message
    {
        return new Message(
            $message->topic_name,
            $this->formatterFactory->instance($this->getRDMessageFormat($message))->decode($message->payload),
            $message->headers ?? [],
            [
                'key' => $message->key ?? null,
                'timestamp' => $message->timestamp ?? null,
                'partition' => $message->partition ?? null,
                'offset' => $message->offset ?? null,
            ]
        );
    }

    /**
     * @param RDMessage $message
     *
     * @return string
     */
    protected function getRDMessageFormat(RDMessage $message): string
    {
        switch (true) {
            case isset($message->headers)
                && isset($message->headers['format'])
                && is_string($message->headers['format'])
                && $this->formatterFactory->hasFormat($message->headers['format']):
                return $message->headers['format'];
            case isset($this->config['format'])
                && is_string($this->config['format'])
                && $this->formatterFactory->hasFormat($this->config['format']):
                return $this->config['format'];
            default;
                return $this->formatterFactory->defaultFormatter;
        }
    }
}
