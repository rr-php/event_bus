<?php

namespace RR\EventBus\Transports\Kafka;

use RdKafka\Conf;
use RdKafka\TopicConf;
use RdKafka\Producer as RDProducer;
use RR\EventBus\Formatters\FormatterFactory;
use RR\EventBus\Transports\AbstractProducer;

/**
 * Class Producer
 * @package RR\EventBus\Transports\Kafka
 */
class Producer extends AbstractProducer
{
    use KafkaConfiguration;

    /**
     * @var Conf $config
     */
    protected $RDKafkaGlobalConfiguration;

    /**
     * @var TopicConf $RDKafkaTopicConfiguration
     */
    protected $RDKafkaTopicConfiguration;

    /**
     * @var RDProducer $RDProducer
     */
    protected $RDProducer;

    /**
     * @var FormatterFactory $formatterFactory
     */
    protected $formatterFactory;

    /**
     * Consumer constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        parent::__construct($config);

        $this->RDKafkaGlobalConfiguration = $this->getGlobalConfiguration($this->config);
        $this->RDKafkaTopicConfiguration = $this->getTopicConfiguration($this->config);

        if (function_exists('pcntl_sigprocmask')) {
            pcntl_sigprocmask(SIG_BLOCK, [SIGIO]);
            $this->RDKafkaGlobalConfiguration->set('internal.termination.signal', SIGIO);
        } else {
            $this->RDKafkaGlobalConfiguration->set('queue.buffering.max.ms', 1);
        }

        $this->RDKafkaGlobalConfiguration->setErrorCb(function ($kafka, $err, $reason) {
            throw new \ErrorException(sprintf('Kafka message error: %s; code: %d', $reason, $err));
        });

        $this->RDProducer = new RDProducer($this->RDKafkaGlobalConfiguration);
        $this->formatterFactory = new FormatterFactory();
    }

    /**
     * @param string $route
     * @param mixed $message
     * @param string $key
     * @param array $headers
     * @param string $format
     *
     * @return void
     */
    public function produce(
        string $route,
        $message,
        string $key = null,
        array $headers = null,
        string $format = null
    )
    {
        $route = $this->formatRoute($route);

        if ($format && $this->formatterFactory->hasFormat($format)) {
            /** message format is set manually */
            $headers['format'] = $format;
            $formatter = $this->formatterFactory->instance($format);
        } elseif (isset($this->config['format'])
            && is_string($this->config['format'])
            && $this->formatterFactory->hasFormat($this->config['format'])) {
            /** message format is set from configuration */
            $formatter = $this->formatterFactory->instance($this->config['format']);
        } else {
            /** default message format */
            $formatter = $this->formatterFactory->instance($this->formatterFactory->defaultFormatter);
        }
        $message = $formatter->encode($message);

        $this->RDKafkaTopicConfiguration->setPartitioner(
            $key
                ? RD_KAFKA_MSG_PARTITIONER_CONSISTENT
                : RD_KAFKA_MSG_PARTITIONER_RANDOM
        );
        $producerTopic = $this->RDProducer->newTopic($route, $this->RDKafkaTopicConfiguration);

        $headers['created_at'] = time();

        $producerTopic->producev(RD_KAFKA_PARTITION_UA, 0, $message, $key, $headers);

        while ($this->RDProducer->getOutQLen() > 0) {
            $this->RDProducer->poll(1);
        }
    }
}
