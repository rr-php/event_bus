<?php

namespace RR\EventBus\Transports;

use Exception;
use RR\EventBus\BusRouter;
use RR\EventBus\ConsumerInterface;
use RR\EventBus\Message;
use RR\EventBus\MessageProcessorInterface;

/**
 * Class AbstractConsumer
 * @package RR\EventBus\Transports
 */
abstract class AbstractConsumer extends AbstractTransport implements ConsumerInterface
{
    /**
     * @param Message $message
     * @param BusRouter $busRouter
     *
     * @return bool
     * @return void
     * @throws Exception
     */
    public function processMessage(Message $message, BusRouter $busRouter)
    {
        $prefix = $this->getPrefix();

        // remove prefix from topic name
        if ($prefix && substr($message->topic, 0, strlen($prefix)) == $prefix) {
            $message->topic = substr($message->topic, strlen($prefix));
        }

        $processorClass = $busRouter->get($message->topic);
        /** @var MessageProcessorInterface $processor */
        $processor = ($creatorCallable = $busRouter->handlerCreator)
            ? $creatorCallable($processorClass)
            : new $processorClass();

        if ($processor instanceof MessageProcessorInterface) {
            return $processor->process($message);
        } else {
            throw new Exception($processorClass . ' is not instance of ' . MessageProcessorInterface::class);
        }
    }

    /**
     * @param BusRouter $busRouter
     * @return array
     */
    public function getTransportRoutes(BusRouter $busRouter)
    {
        $prefix = $this->getPrefix();
        $routesWithPrefixes = [];

        foreach ($busRouter->getRoutes() as $route) {
            $routesWithPrefixes[] = $prefix . $route;
        }

        return $routesWithPrefixes;
    }
}
