<?php

namespace RR\EventBus\Transports\Null;

use RR\EventBus\ProducerInterface;

/**
 * Class Producer
 * @package RR\EventBus\Transports\Null
 */
class Producer implements ProducerInterface
{

    /**
     * @inheritDoc
     */
    public function __construct(array $config)
    {
    }

    /**
     * @inheritDoc
     */
    public function produce(string $route, $message)
    {
        // TODO: Implement produce() method.
    }
}
