<?php

namespace RR\EventBus\Transports\Null;

use RR\EventBus\BusRouter;
use RR\EventBus\ConsumerInterface;
use RR\EventBus\Message;

/**
 * Class Consumer
 * @package RR\EventBus\Transports\Null
 */
class Consumer implements ConsumerInterface
{
    /**
     * @inheritDoc
     */
    public function __construct(array $config)
    {
    }

    /**
     * @inheritDoc
     */
    public function consume(BusRouter $busRouter)
    {
        // TODO: Implement consume() method.
    }

    /**
     * @inheritDoc
     */
    public function processMessage(Message $message, BusRouter $busRouter)
    {
        // TODO: Implement processMessage() method.
    }
}
