<?php

namespace RR\EventBus\Transports;

/**
 * Class AbstractTransport
 * @package RR\EventBus\Transports
 */
abstract class AbstractTransport
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var string
     */
    private $prefix = '';

    /**
     * AbstractConsumer constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->prefix = $this->config['prefix'] ?? '';
    }

    /**
     * @return mixed|string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }
}
