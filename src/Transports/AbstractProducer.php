<?php

namespace RR\EventBus\Transports;

use RR\EventBus\ProducerInterface;

/**
 * Class AbstractProducer
 * @package RR\EventBus\Transports
 */
abstract class AbstractProducer extends AbstractTransport implements ProducerInterface
{
    /**
     * @param string $route
     * @return string
     */
    public function formatRoute(string $route)
    {
        return $this->getPrefix() . $route;
    }
}
