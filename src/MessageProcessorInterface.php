<?php

namespace RR\EventBus;

/**
 * Interface MessageProcessorInterface
 * @package RR\EventBus
 */
interface MessageProcessorInterface
{
    /**
     * @param Message $message
     * @return bool
     */
    public function process(Message $message): bool;
}
