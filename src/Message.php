<?php

namespace RR\EventBus;

/**
 * Class Message
 * @package RR\EventBus
 */
class Message
{
    /**
     * @var string
     */
    public $topic;

    /**
     * @var mixed
     */
    public $payload;

    /**
     * @var array
     */
    public $headers;

    /**
     * @var array
     */
    public $meta;

    /**
     * Message constructor.
     * @param string $topic
     * @param mixed $payload
     * @param array $headers
     * @param array $meta
     */
    public function __construct(
        string $topic,
        $payload,
        array $headers = [],
        array $meta = []
    )
    {
        $this->topic = $topic;
        $this->payload = $payload;
        $this->headers = $headers;
        $this->meta = $meta;
    }
}
