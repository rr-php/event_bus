<?php

namespace RR\EventBus;

/**
 * Interface ConsumerInterface
 * @package RR\EventBus
 */
interface ConsumerInterface
{
    /**
     * ConsumerInterface constructor.
     *
     * @param array $config
     */
    public function __construct(array $config);

    /**
     * @param BusRouter $busRouter
     *
     * @return mixed
     */
    public function consume(BusRouter $busRouter);

    /**
     * @param Message $message
     * @param BusRouter $busRouter
     *
     * @return mixed
     */
    public function processMessage(Message $message, BusRouter $busRouter);
}
