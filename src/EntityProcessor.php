<?php

namespace RR\EventBus;

/**
 * Class EntityProcessor
 * @package RR\EventBus
 */
abstract class EntityProcessor implements MessageProcessorInterface
{
    /**
     * @var Message
     */
    protected $message;

    /**
     * @param Message $message
     * @return bool
     */
    public function process(Message $message): bool
    {
        $this->message = $message;
        $method = $message->payload['method'] ?? null;
        $data = $message->payload['data'] ?? null;
        $methods = $this->getMethods();
        if (isset($methods[$method])) {
            return $this->{$methods[$method]}($data);
        } else {
            return $this->unknownMethod($message);
        }
    }

    /**
     * @return array
     */
    abstract function getMethods(): array;

    /**
     * @param Message $message
     * @return bool
     */
    protected function unknownMethod(Message $message)
    {
        // rewrite to catch it
        return true;
    }
}