## Install

```bash
apt-get update -y && apt-get install -y \
    librdkafka++1=0.11.6-1.1 \
    librdkafka1=0.11.6-1.1 \
    librdkafka-dev=0.11.6-1.1 \
    
pecl install -f rdkafka-4.0.3 && \
    echo "extension=rdkafka.so" >> /path/to/php.ini
```

## Example consumer

```php

$router = new \RR\EventBus\BusRouter();

// Handler instance of \RR\EventBus\MessageProcessorInterface::class
$router->add('test_topic', \App\Handler::class); 

$client = new \RR\EventBus\Transports\Kafka\Consumer(
    [
        // https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
        'rd_kafka_conf' => [
            'metadata.broker.list' => 'kafka:9092', // broker host:port
            'group.id' => 'api', //group ID(should be same for all service instances). Example: api OR search 
            'auto.offset.reset' => 'smallest',
            'offset.store.method' => 'broker',
            'queued.max.messages.kbytes' => '100000',
            'topic.metadata.refresh.sparse' => 'true',
            'topic.metadata.refresh.interval.ms' => '600000',
            'auto.commit.enable' => 'false'
        ],
        'timeout' => 1000*120,
        'prefix' => 'api_' // optional
    ]
);

$client->consume($router);
```
## Example producer

```php

$client = new \RR\EventBus\Transports\Kafka\Producer(
    [
        // https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
        'rd_kafka_conf' => [
            'metadata.broker.list' => 'kafka:9092', // broker host:port
            'group.id' => 'api', //group ID(should be same for all service instances). Example: api OR search 
            'auto.offset.reset' => 'smallest',
            'offset.store.method' => 'broker',
            'queued.max.messages.kbytes' => '100000',
            'topic.metadata.refresh.sparse' => 'true',
            'topic.metadata.refresh.interval.ms' => '600000',
            'auto.commit.enable' => 'false'
        ],
        'timeout' => 1000*120,
        'prefix' => 'api_' // optional
    ]
);


$client->produce(
    // required, string
    'topic_name',
    
    // required, mixed
    ['some_message'],
    
    // optional, string, should be set to ensure messages with the same key will be executed in same order as the were produced
    'key', 
    
    // optional, array, key "format" is reserved, do not use it
    [
        'header_key' => 'header_value'
    ],
    
    // optional, string, message format
    'json'
);
```



